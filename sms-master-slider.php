<?php
/*
	Section: SMS Master Slider
  Author: Roadside Multimedia
	Contributor: Milo Jennings
  Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-master-slider
	Description: Displays Master Slider slideshows
	Class Name: sms_master_slider
  Section: Master Slider
	Filter: slider, gallery
  Text Domain: dms-sms
	Loading: active
  Version: 1.0.0
  Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-master-slider
  Bitbucket Branch: master
*/


class sms_master_slider extends PageLinesSection {

	function section_opts(){

    $slider_id = get_masterslider_names(false);
    $slider_names = get_masterslider_names();
    $slider_array = array_combine($slider_id, $slider_names);

    $slider_opt_array = array();
    foreach ($slider_array as $id => $name) {
      $slider_opt_array[$id] = array( 'name' => $name );
    }

    $opts = array(

      array(
        'type'  => 'multi',
        'key' => 'dms_master_slider_config',
        'title' => 'Text',
        'col' => 1,
        'opts'  => array(
          array(
            'type'  => 'select',
            'key' => 'dms_master_slideshow',
            'label' => 'Choose a slideshow',
            'opts'  => $slider_opt_array
          ),
        ),
      ),
    );
    // echo "<pre>" . print_r($opts[0]['opts'], true) . "</pre>";

		return $opts;

	}


  function section_template( $location = false ) {

    $slider_id = ( $this->opt('dms_master_slideshow') ) ? $this->opt( 'dms_master_slideshow' ) : 'fail!';

    masterslider ( $slider_id );

    // echo "<pre>" . print_r($slider_opt_array, true) . "</pre>";

  }

}
